<?php
class payment extends Eloquent {
        protected $guarded = array();
        protected $table = 'cardpayment'; // table name
        public $timestamps = 'false' ; // to disable default timestamp fields

        // model function to store form data to database
        public static function saveFormData($data)
        {
            DB::table('cardpayment')->insert($data);
        }

}

?>
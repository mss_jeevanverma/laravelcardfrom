@extends('layouts/layout')

@section('content')


    <style>

.main-content {
    box-shadow: 0 0 9px #cecece;
    margin: 0 auto;
    padding: 20px;
    width: 45%;
}

.main-content h1 {
    border-bottom: 1px solid #ddd;
    font-size: 26px;
    margin: 0 0 25px;
    padding-bottom: 14px;
}

.login-wrap label {
    color: #646464;
    font-size: 14px;
    font-weight: normal;
    width: 100%;
}

.login-wrap input {
    border: 1px solid #ddd;
    font-size: 12px;
    height: 40px;
    padding-left: 12px;
    width: 100%;
}

.login-wrap select {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #ddd;
    color: #919191;
    font-size: 12px;
    height: 40px;
    padding-left: 12px;
    padding-top: 11px;
    width: 100%;
}

.sbmtbtn {
    margin-top: 20px;
    padding: 9px 50px;
}


.form-row span {
    float: left;
    margin-bottom: 8px;
    width: 100% !important;
}

.left {
    width: 15% !important;
}

.right {
    width: 15% !important;
}



    </style>


   
    <div class="main-content" ng-app="Cardapp">
    <h1> CREDIT CARD FORM </h1>
    <div class="alert alert-danger alert-dismissable" style="display:none">ffff</div>
    <div class="sucess alert-success alert-dismissable " style="display:none">tetttt </div>
    <div id="create"  ng-controller="cardController" ng-submit="submit()">
                   <form method="post">
                                  <div class="login-wrap">
                                <p> 
                                    <label for="firstname"  data-icon="u">First Name</label>
                                    <input  name="firstname" ng-model="firstname" required type="text" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,100}$" title="enter less then 100 character" placeholder="Enter your Firstname" />
                                </p>
                                <p> 
                                    <label for="lastname" > Last Name</label>
                                    <input name="lastname"  ng-model="lastname" required type="text" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,100}$" title="enter less then 100 character" placeholder="Enter your Lastname"/> 
                                </p>
                                <p> 
                                    <label for="address" > Address</label>
                                    <input name="address"  ng-model="address" required type="text" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,100}$" title="enter less then 100 character" placeholder="Enter your Address"/>
                                </p>
                               <p> 
                                    <label for="address" > Postcode</label>
                                    <input name="postcode"  ng-model="postcode" required  Pattern="(\d{4}([\-]\d{4})?)" maxlength="4" title="enter 4 digit" type="text" placeholder="Enter your Postcode"/>
                                </p>

                                <p> 

 
                                    <label for="state"  > State</label>
                                    <select ng-model="state" >
                                    <option>Select Your state</option>
                                    <option value="ACT">ACT</option>
                                    <option value="NSW">NSW</option>
                                    <option value="NT">NT</option>
                                    <option value="QLD">QLD</option>
                                    <option value="SA">SA</option>
                                    <option value="TAS">TAS</option>
                                    <option value="VIC">VIC</option>
                                    <option value="WA">WA</option>
                                    </select>
                                </p>
                                  <p> 
                                    <label for="cartnumber"  > Credit card number</label>
                                    <input name="cartnumber"  ng-model="cartnumber" pattern="[0-9]{13,16}" required type="text" placeholder="Enter your Credit card number"/>
                                </p>
                                <div class="form-row">
                                <label>
                                  <span>Credit card expiry date (MM/YY)</span>
                                  <input type="text"  required ng-model="month" size="2" data-stripe="exp-month" title="enter expiry month" pattern="[0-9]{2}" class="left"> /
                                  <input type="text" required  ng-model="year" size="4" data-stripe="exp-year" title="enter expiry year" pattern="[0-9]{2}" class="right" />
                                </label>                                
                                
                              </div>
                                </div>
    
                    <p>{{ Form::submit('Submit', array('class' => 'sbmtbtn btn btn-primary')) }}</p>
                    </form>
    </div>
    </div>

@stop
<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function card(){
		$data =  Input::except(array('_token')) ;
		$rule = array(
				'firstname'  =>  'required|AlphaNum|max:100',
                'lastname'   => 'required|max:100',
                'address'    => 'required|max:100',
                'postcode'   => 'required|numeric|max:4',
                'state'      => 'required',
                'cartnumber' => 'required|unique:cardpayment',
                'month'      => 'required',
                'year'       => 'required',
				);

		/****
		 * Server side data validation handler @REST api
		 */
		if($data){

			/***
			 * Validating first name from UI post data
			 */
			if(($data['firstname']==null)||(strlen($data['firstname']) > 100)){
				return json_encode(array("error"=>true,"message"=>"The firstname is required field, min 100 characters allowed."));
			}

			/***
			 * Validating first name from UI post data
			 */
			if(($data['lastname']==null)||(strlen($data['lastname']) > 100)){
				return json_encode(array("error"=>true,"message"=>"The lastname is required field, min 100 characters allowed."));
			}

			/***
			 * Validating first name from UI post data
			 */
			if(($data['lastname']==null)||(strlen($data['lastname']) > 100)){
				return json_encode(array("error"=>true,"message"=>"The lastname is required field, min 100 characters allowed."));
			}

		}

		$validator = Validator::make($data, $rule);

		if ($validator->fails()) {
				$messages = $validator->messages();
				return json_encode(array("error"=>true,"message"=>$messages));
		 } else {
		 	$newpayment = new payment;
		 	$newpayment->firstname  = Input::get('firstname');
			$newpayment->lastname  = Input::get('lastname');
			$newpayment->address = Input::get('address');
			$newpayment->postcode  = Input::get('postcode');
			$newpayment->state  = Input::get('state');
			$newpayment->cartnumber  = Input::get('cartnumber');
			$newpayment->month  = Input::get('month');
			$newpayment->year  = Input::get('year');
		    //$newpayment->save();
		    echo "sucessfuly insert value";
		 }
		 //print_r($data);
	}

}

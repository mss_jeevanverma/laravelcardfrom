<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardpaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cardpayment', function($table)
    {
        $table->increments('id');
        $table->string('firstname');
        $table->string('lastname');
        $table->string('address');
        $table->char('postcode', 4);
        $table->string('state');
        $table->text('cartnumber', 20);
        $table->char('month', 2);
        $table->char('year', 2); 
        $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('cardpayment');
	}

}
